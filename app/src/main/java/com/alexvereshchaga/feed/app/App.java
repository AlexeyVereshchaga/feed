package com.alexvereshchaga.feed.app;

import android.app.Application;
import android.content.Context;

import com.alexvereshchaga.feed.di.component.ApplicationComponent;

import com.alexvereshchaga.feed.di.component.DaggerApplicationComponent;
import com.alexvereshchaga.feed.di.module.ApplicationModule;


public class App extends Application {

    public static App mInstance;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    // Needed to replace the component with a test
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }


}
