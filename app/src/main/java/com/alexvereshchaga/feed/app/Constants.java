package com.alexvereshchaga.feed.app;



public class Constants {

    public static final int CONNECT_TIMEOUT = 30;
    public static final int WRITE_TIMEOUT = 30;
    public static final int TIMEOUT = 30;

    public static final int START_SETTINGS = 32000;

    public static final int ACTION_CODE = 10000;


}
