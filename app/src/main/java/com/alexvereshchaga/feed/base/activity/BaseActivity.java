package com.alexvereshchaga.feed.base.activity;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alexvereshchaga.feed.base.mvp.IBaseProgressView;
import com.alexvereshchaga.feed.base.screencreator.ScreenCreator;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.progressdialog.ProgressDialog;
import com.alexvereshchaga.feed.progressdialog.ProgressDialogManager;
import com.alexvereshchaga.feed.rest.error.RetrofitException;
import com.alexvereshchaga.feed.view.progress.ProgressView;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseActivity extends AppCompatActivity implements IBaseProgressView, ProgressDialog.OnOkButtonClickCallbackListener {

    protected ProgressView mProgressView;

    private Unbinder mUnbinder;

    protected boolean isSavedInstanceStateDone;
    protected boolean isMakeStartRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mUnbinder = ButterKnife.bind(this);
        initProgressLayout();
    }

    private void initProgressLayout() {
        mProgressView = getProgressView();
        if (mProgressView != null) {
            mProgressView.setOnRetryListener(new ProgressView.OnRetryListener() {
                @Override
                public void onRetry() {
                    retry();
                }
            });
        }
    }

    protected abstract int getLayout();

    protected abstract ProgressView getProgressView();

    protected void retry() {
    }

    @Override
    public void startProgressDialog() {
        if (!isSavedInstanceStateDone()) {
            ProgressDialogManager.getInstance().startLoading(this);
            isMakeStartRequest = false;
        } else {
            isMakeStartRequest = true;
        }
    }

    @Override
    public void completeProgressDialog() {
        ProgressDialogManager.getInstance().completeLoading();
    }

    @Override
    public void errorProgressDialog(ErrorData errorData) {
        errorProgressDialog(null, errorData);
    }

    @Override
    public void errorProgressDialog(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData) {
        if (errorData != null && !(errorData.getErrorCode() >= 500 && errorData.getErrorCode() <= 520)) {
            ProgressDialogManager.getInstance().errorLoading(onOkButtonClickCallbackListener, errorData);
        } else {
            ProgressDialogManager.getInstance().errorLoading(this, errorData);
        }
    }

    @Override
    public void errorProgressDialog(RetrofitException retrofitException) {
        errorProgressDialog(null, retrofitException);
    }

    @Override
    public void errorProgressDialog(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException) {
        ProgressDialogManager.getInstance().errorLoading(onOkButtonClickCallbackListener, retrofitException);
    }

    @Override
    public void startLoading() {
        if (mProgressView != null) {
            mProgressView.startLoading();
        } else {
            throw new ProgressViewNotAttachedException();
        }
    }

    @Override
    public void completeLoading() {
        if (mProgressView != null) {
            mProgressView.completeLoading();
        } else {
            throw new ProgressViewNotAttachedException();
        }
    }

    @Override
    public void errorLoading(ErrorData errorData) {
        if (mProgressView != null) {
            mProgressView.errorLoading(errorData);
        } else {
            throw new ProgressViewNotAttachedException();
        }
    }

    @Override
    public void errorLoading(RetrofitException retrofitException) {
        if (mProgressView != null) {
            mProgressView.errorLoading(retrofitException);
        } else {
            throw new ProgressViewNotAttachedException();
        }
    }

    protected ScreenCreator getScreenCreator() {
        return ScreenCreator.getInstance();
    }


    @Override
    public void onResume() {
        super.onResume();
        isSavedInstanceStateDone = false;
        checkStartProgressDialog();
    }

    private void checkStartProgressDialog() {
        if (isMakeStartRequest) {
            startProgressDialog();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSavedInstanceStateDone = true;
    }

    public boolean isSavedInstanceStateDone() {
        return isSavedInstanceStateDone;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onOkButtonClick(int errorCode) {
// TODO: 12.10.2018
    }

    private static class ProgressViewNotAttachedException extends RuntimeException {
        ProgressViewNotAttachedException() {
            super("Please set progress view not null object");
        }
    }
}