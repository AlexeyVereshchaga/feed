package com.alexvereshchaga.feed.base.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.utils.FieldConverter;
import com.alexvereshchaga.feed.view.progress.ProgressView;

public abstract class BaseContainerActivity extends BaseActivity {

    protected Toolbar mToolbar;
    protected ImageView mIvCancel;
    protected TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar = (Toolbar) findViewById(R.id.navigation_bar);
        mIvCancel = (ImageView) findViewById(R.id.toolbar_cancel);
        mTitleTextView = (TextView) findViewById(R.id.toolbar_title);
        initToolbar();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_container;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitleTextView.setText(title);
    }

    @Override
    public void setTitle(int titleId) {
        mTitleTextView.setText(FieldConverter.getString(titleId));
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    protected abstract void openFragment();

}
