package com.alexvereshchaga.feed.base.adapter;

public interface ILoadMoreListener {

    void loadData(int currentPage);

}
