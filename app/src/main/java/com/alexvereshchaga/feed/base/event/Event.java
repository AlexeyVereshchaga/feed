package com.alexvereshchaga.feed.base.event;


public abstract class Event {

    protected int mActionCode;

    public Event(int mActionCode) {
        this.mActionCode = mActionCode;
    }

    public int getActionCode() {
        return mActionCode;
    }

    public void setActionCode(int mActionCode) {
        this.mActionCode = mActionCode;
    }


    public abstract EventType getEventType();

}
