package com.alexvereshchaga.feed.base.event;

import com.alexvereshchaga.feed.rest.error.RetrofitException;

public class EventFailRequest extends Event {

    private RetrofitException mThrowable;
    private String mMessage;

    public EventFailRequest(int mActionCode, RetrofitException mThrowable, String message) {
        super(mActionCode);
        this.mThrowable = mThrowable;
        this.mMessage = message;
    }

    public RetrofitException getThrowable() {
        return mThrowable;
    }

    public void setThrowable(RetrofitException mThrowable) {
        this.mThrowable = mThrowable;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    @Override
    public EventType getEventType() {
        return EventType.FAIL_REQUEST;
    }

}
