package com.alexvereshchaga.feed.base.event;


public class EventFinishRequest extends Event {

    public EventFinishRequest(int actionCode) {
        super(actionCode);
    }

    @Override
    public EventType getEventType() {
        return EventType.FINISH_REQUEST;
    }

}
