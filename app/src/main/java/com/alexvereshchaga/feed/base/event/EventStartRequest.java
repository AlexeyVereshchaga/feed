package com.alexvereshchaga.feed.base.event;

public class EventStartRequest extends Event {

    public EventStartRequest(int mActionCode) {
        super(mActionCode);
    }

    @Override
    public EventType getEventType() {
        return EventType.START_REQUEST;
    }

}
