package com.alexvereshchaga.feed.base.event;

import retrofit2.Response;



public class EventSuccessRequest extends Event {

    private Response mData;

    public EventSuccessRequest(int mActionCode, Response mData) {
        super(mActionCode);
        this.mData = mData;
    }

    public Response getData() {
        return mData;
    }

    public void setData(Response mData) {
        this.mData = mData;
    }

    @Override
    public EventType getEventType() {
        return EventType.SUCCESS_REQUEST;
    }

}
