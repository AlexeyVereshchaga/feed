package com.alexvereshchaga.feed.base.event;



public enum  EventType {

    CUSTOM_EVENT,
    START_REQUEST,
    FINISH_REQUEST,
    SUCCESS_REQUEST,
    FAIL_REQUEST

}
