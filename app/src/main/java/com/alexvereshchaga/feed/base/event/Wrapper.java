package com.alexvereshchaga.feed.base.event;



public class Wrapper<T> {

    private T mValue;

    public Wrapper(T value){
        this.mValue = value;
    }

    public Object getValue() {
        return mValue;
    }

    public void setValue(T value) {
        this.mValue = value;
    }

}
