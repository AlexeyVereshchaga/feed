package com.alexvereshchaga.feed.base.mvp;

import com.alexvereshchaga.feed.eventbus.EventBusController;
import com.alexvereshchaga.feed.eventbus.IEventObserver;

import javax.inject.Inject;




public abstract class BaseEventBusPresenter<T extends IBaseMvpView> extends BasePresenter<T> implements IEventObserver {

    @Inject
    protected EventBusController mEventBusController;

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        mEventBusController.addObserver(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        mEventBusController.removeObserver(this);
    }
}
