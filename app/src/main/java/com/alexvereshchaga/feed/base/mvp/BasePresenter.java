package com.alexvereshchaga.feed.base.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.alexvereshchaga.feed.managers.DataManager;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;



public abstract class BasePresenter<T extends IBaseMvpView> implements IBaseMvpPresenter<T> {

    protected T mMvpView;
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    protected DataManager mDataManager;

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        undisposableAll();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        return;
    }

    @Override
    public void setArguments(Object... params) {
        return;
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        return;
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        return;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }


    public T getMvpView() {
        checkViewAttached();
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public void addDisposable(Disposable disposable){
        mCompositeDisposable.add(disposable);
    }

    public void undisposable(Disposable disposable){
        if(mCompositeDisposable.size() != 0){
            if(!disposable.isDisposed()) {
                mCompositeDisposable.remove(disposable);
            }
        }
    }

    public void undisposableAll(){
        if(!mCompositeDisposable.isDisposed()){
            mCompositeDisposable.clear();
            mCompositeDisposable.dispose();
        }
    }

    private static class MvpViewNotAttachedException extends RuntimeException {
        MvpViewNotAttachedException() {
            super("Please call OffersListPresenter.attachView(MvpView) before" +
                    " requesting data to the OffersListPresenter");
        }
    }
}
