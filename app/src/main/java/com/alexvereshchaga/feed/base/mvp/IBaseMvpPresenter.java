package com.alexvereshchaga.feed.base.mvp;

import android.os.Bundle;



public interface IBaseMvpPresenter<V extends IBaseMvpView> {

    void attachView(V mvpView);

    void detachView();

    void onCreate(Bundle savedInstanceState);

    void setArguments(Object... params);

    void saveInstanceState(Bundle outState);

    void restoreInstanceState(Bundle savedInstanceState);
}
