package com.alexvereshchaga.feed.base.mvp;

import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.progressdialog.ProgressDialog;
import com.alexvereshchaga.feed.rest.error.RetrofitException;



public interface IBaseProgressView extends IBaseMvpView{

    void startProgressDialog();
    void completeProgressDialog();
    void errorProgressDialog(ErrorData errorData);
    void errorProgressDialog(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData);
    void errorProgressDialog(RetrofitException retrofitException);
    void errorProgressDialog(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException);

    void startLoading();
    void completeLoading();
    void errorLoading(ErrorData errorData);
    void errorLoading(RetrofitException retrofitException);

}
