package com.alexvereshchaga.feed.base.request;

import com.alexvereshchaga.feed.eventbus.IEventSubject;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

import retrofit2.Response;

public class BaseRequestController implements IRequestCallback{

    private IEventSubject mObservable;
    private int mActionCode;

    public BaseRequestController(IEventSubject subject, int actionCode){
        this.mObservable = subject;
        this.mActionCode = actionCode;
    }

    @Override
    public void onStartRequest() {
        mObservable.notifyStartedWithAction(mActionCode);
    }

    @Override
    public void onFinishRequest() {
        mObservable.notifyFinishWithAction(mActionCode);
    }

    @Override
    public void onErrorResponse(RetrofitException e, String message) {
        mObservable.notifyFailed(mActionCode, e, message);
    }

    @Override
    public void onSuccessResponse(Response response) {
        mObservable.notifySuccess(mActionCode, response);
    }
}
