package com.alexvereshchaga.feed.base.request;

import com.alexvereshchaga.feed.rest.error.RetrofitException;

import retrofit2.Response;



public interface IRequestCallback {

    void onStartRequest();

    void onFinishRequest();

    void onErrorResponse(RetrofitException e, String message);

    void onSuccessResponse(Response response);

}
