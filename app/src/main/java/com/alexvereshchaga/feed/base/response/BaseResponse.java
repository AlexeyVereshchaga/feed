package com.alexvereshchaga.feed.base.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BaseResponse implements Parcelable {

    @SerializedName("code")
    private int code;
    @SerializedName("error")
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int mResult) {
        this.code = mResult;
    }

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public BaseResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeByte((byte) (error ? 1 : 0));
    }

    protected BaseResponse(Parcel in) {
        this.code = in.readInt();
        this.error = in.readByte() != 0;
    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel source) {
            return new BaseResponse(source);
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };
}
