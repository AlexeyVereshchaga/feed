package com.alexvereshchaga.feed.base.response;

public enum ErrorKind {

    BAD_REQUEST_ERROR,
    UNAUTHORIZED_ERROR,
    DEFAULT_ERROR,
}
