package com.alexvereshchaga.feed.base.response;

import com.alexvereshchaga.feed.base.event.Wrapper;



public interface IBaseResponseCallback {

    void onSuccess(int actionCode, int errorCode, Wrapper data);

    void onError(ErrorKind errorKind, int actionCode, int errorCode, String description);
}
