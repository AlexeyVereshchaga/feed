package com.alexvereshchaga.feed.base.response;

import com.alexvereshchaga.feed.base.event.Wrapper;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

import retrofit2.Response;

public class ResponseHandler {

    public static final int SUCCESS_CODE = 200;
    public static final int BAD_REQUEST_ERROR_CODE = 1;

    private static ResponseHandler mInstance;

    private ResponseHandler() {
    }

    public static ResponseHandler newInstance() {
        if (mInstance == null) {
            mInstance = new ResponseHandler();
        }
        return mInstance;
    }

    public <V extends IBaseResponseCallback> void handle(int actionCode, Response response, V baseResponseCallback) {
        if (response != null) {
            if (response.isSuccessful()) {
                BaseResponse baseResponse = (BaseResponse) response.body();
                switch (baseResponse.getCode()) {
                    case SUCCESS_CODE:
                        if (baseResponseCallback != null) {
                            baseResponseCallback.onSuccess(actionCode, baseResponse.getCode(), new Wrapper(baseResponse));
                        }
                        break;
                    default:
                        if (baseResponseCallback != null) {
                            baseResponseCallback.onError(ErrorKind.DEFAULT_ERROR, actionCode, baseResponse.getCode(), null);
                        }
                        break;
                }
            } else {
                if (baseResponseCallback != null) {
                    if (response.code() == 400) {
                        baseResponseCallback.onError(ErrorKind.BAD_REQUEST_ERROR, actionCode, BAD_REQUEST_ERROR_CODE, null);
                    } else {
                        baseResponseCallback.onError(ErrorKind.DEFAULT_ERROR, actionCode, RetrofitException.ERROR_UNEXPECTED, null);
                    }
                }
            }
        }
    }
}