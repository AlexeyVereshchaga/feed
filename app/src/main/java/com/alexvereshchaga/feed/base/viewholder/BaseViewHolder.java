package com.alexvereshchaga.feed.base.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;



public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    protected final View mItemView;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mItemView = itemView;
    }

}
