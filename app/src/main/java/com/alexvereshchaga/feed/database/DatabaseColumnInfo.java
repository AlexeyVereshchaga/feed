package com.alexvereshchaga.feed.database;

public interface DatabaseColumnInfo {

    String TABLE_POST = "post";

    String POST_ID = "id";
    String POST_TEXT = "TEXT";
}
