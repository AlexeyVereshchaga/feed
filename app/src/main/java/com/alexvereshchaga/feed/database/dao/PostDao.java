package com.alexvereshchaga.feed.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.alexvereshchaga.feed.database.DatabaseColumnInfo;
import com.alexvereshchaga.feed.dataclasses.Post;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPost(Post post);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPosts(List<Post> posts);

    @Query("SELECT * FROM " + DatabaseColumnInfo.TABLE_POST)
    Single<List<Post>> getAllPosts();

    @Query("DELETE FROM " + DatabaseColumnInfo.TABLE_POST)
    void deleteAllPosts();

    @Query("SELECT * FROM " + DatabaseColumnInfo.TABLE_POST + " WHERE " + DatabaseColumnInfo.POST_TEXT + " LIKE " + ":query")
    Single<List<Post>> getPostsByQuery(String query);

    @Query("SELECT * FROM " + DatabaseColumnInfo.TABLE_POST + " WHERE " + DatabaseColumnInfo.POST_ID + " =:id")
    Single<Post> getPostById(String id);
}
