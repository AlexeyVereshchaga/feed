
package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dislikes implements Parcelable {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("my")
    @Expose
    private Integer my;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMy() {
        return my;
    }

    public void setMy(Integer my) {
        this.my = my;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.count);
        dest.writeValue(this.my);
    }

    public Dislikes() {
    }

    protected Dislikes(Parcel in) {
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
        this.my = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Dislikes> CREATOR = new Parcelable.Creator<Dislikes>() {
        @Override
        public Dislikes createFromParcel(Parcel source) {
            return new Dislikes(source);
        }

        @Override
        public Dislikes[] newArray(int size) {
            return new Dislikes[size];
        }
    };
}
