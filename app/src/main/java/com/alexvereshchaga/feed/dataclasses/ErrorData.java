package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ErrorData implements Parcelable {

    private int errorCode;
    private String errorMessage;
    private int errorDrawable;

    public ErrorData(String mErrorMessage) {
        this.errorMessage = mErrorMessage;
    }

    public ErrorData(int mErrorCode) {
        this.errorCode = mErrorCode;
    }

    public ErrorData(String mErrorMessage, int mErrorDrawable) {
        this.errorMessage = mErrorMessage;
        this.errorDrawable = mErrorDrawable;
    }

    public ErrorData(int mErrorCode, String mErrorMessage, int mErrorDrawable) {
        this.errorCode = mErrorCode;
        this.errorMessage = mErrorMessage;
        this.errorDrawable = mErrorDrawable;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int mErrorCode) {
        this.errorCode = mErrorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String mErrorMessage) {
        this.errorMessage = mErrorMessage;
    }

    public int getErrorDrawable() {
        return errorDrawable;
    }

    public void setErrorDrawable(int mErrorDrawable) {
        this.errorDrawable = mErrorDrawable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.errorCode);
        dest.writeString(this.errorMessage);
        dest.writeInt(this.errorDrawable);
    }

    protected ErrorData(Parcel in) {
        this.errorCode = in.readInt();
        this.errorMessage = in.readString();
        this.errorDrawable = in.readInt();
    }

    public static final Creator<ErrorData> CREATOR = new Creator<ErrorData>() {
        @Override
        public ErrorData createFromParcel(Parcel source) {
            return new ErrorData(source);
        }

        @Override
        public ErrorData[] newArray(int size) {
            return new ErrorData[size];
        }
    };
}
