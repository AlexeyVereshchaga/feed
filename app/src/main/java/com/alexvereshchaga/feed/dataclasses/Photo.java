
package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo implements Parcelable {

    @SerializedName("photo_big")
    @Expose
    private String photoBig;
    @SerializedName("photo_medium")
    @Expose
    private String photoMedium;
    @SerializedName("photo_small")
    @Expose
    private String photoSmall;
    @SerializedName("size")
    @Expose
    private Size size;

    public String getPhotoBig() {
        return photoBig;
    }

    public void setPhotoBig(String photoBig) {
        this.photoBig = photoBig;
    }

    public String getPhotoMedium() {
        return photoMedium;
    }

    public void setPhotoMedium(String photoMedium) {
        this.photoMedium = photoMedium;
    }

    public String getPhotoSmall() {
        return photoSmall;
    }

    public void setPhotoSmall(String photoSmall) {
        this.photoSmall = photoSmall;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.photoBig);
        dest.writeString(this.photoMedium);
        dest.writeString(this.photoSmall);
        dest.writeParcelable(this.size, flags);
    }

    public Photo() {
    }

    protected Photo(Parcel in) {
        this.photoBig = in.readString();
        this.photoMedium = in.readString();
        this.photoSmall = in.readString();
        this.size = in.readParcelable(Size.class.getClassLoader());
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
