
package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoBig implements Parcelable {

    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.width);
        dest.writeValue(this.height);
    }

    public PhotoBig() {
    }

    protected PhotoBig(Parcel in) {
        this.width = (Integer) in.readValue(Integer.class.getClassLoader());
        this.height = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<PhotoBig> CREATOR = new Parcelable.Creator<PhotoBig>() {
        @Override
        public PhotoBig createFromParcel(Parcel source) {
            return new PhotoBig(source);
        }

        @Override
        public PhotoBig[] newArray(int size) {
            return new PhotoBig[size];
        }
    };
}
