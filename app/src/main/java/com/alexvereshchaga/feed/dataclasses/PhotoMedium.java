
package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoMedium implements Parcelable {

    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.width);
        dest.writeValue(this.height);
    }

    public PhotoMedium() {
    }

    protected PhotoMedium(Parcel in) {
        this.width = (Integer) in.readValue(Integer.class.getClassLoader());
        this.height = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<PhotoMedium> CREATOR = new Parcelable.Creator<PhotoMedium>() {
        @Override
        public PhotoMedium createFromParcel(Parcel source) {
            return new PhotoMedium(source);
        }

        @Override
        public PhotoMedium[] newArray(int size) {
            return new PhotoMedium[size];
        }
    };
}
