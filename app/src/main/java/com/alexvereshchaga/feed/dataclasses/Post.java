
package com.alexvereshchaga.feed.dataclasses;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.alexvereshchaga.feed.database.DatabaseColumnInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = DatabaseColumnInfo.TABLE_POST)
public class Post implements Parcelable {

    @PrimaryKey
    @ColumnInfo(name = DatabaseColumnInfo.POST_ID)
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("postviews")
    @Expose
    @Ignore
    private Postviews postviews;
    @ColumnInfo(name = DatabaseColumnInfo.POST_TEXT)
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("author_watch")
    @Expose
    private Integer authorWatch;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("adult")
    @Expose
    private Integer adult;
    @SerializedName("hidden")
    @Expose
    private Integer hidden;
    @SerializedName("category")
    @Expose
    private Integer category;
    @SerializedName("filter")
    @Expose
    private Integer filter;
    @SerializedName("category_string")
    @Expose
    private String categoryString;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("open_comments")
    @Expose
    private Integer openComments;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("reposts")
    @Expose
    private Integer reposts;
    @SerializedName("likes")
    @Expose
    @Ignore
    private Likes likes;
    @SerializedName("dislikes")
    @Expose
    @Ignore
    private Dislikes dislikes;
    @SerializedName("comments")
    @Expose
    @Ignore
    private Comments comments;
    @SerializedName("tags")
    @Expose
    @Ignore
    private List<String> tags = null;
    @SerializedName("attachments")
    @Expose
    @Ignore
    private List<Attachment> attachments = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Postviews getPostviews() {
        return postviews;
    }

    public void setPostviews(Postviews postviews) {
        this.postviews = postviews;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Integer getAuthorWatch() {
        return authorWatch;
    }

    public void setAuthorWatch(Integer authorWatch) {
        this.authorWatch = authorWatch;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAdult() {
        return adult;
    }

    public void setAdult(Integer adult) {
        this.adult = adult;
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getFilter() {
        return filter;
    }

    public void setFilter(Integer filter) {
        this.filter = filter;
    }

    public String getCategoryString() {
        return categoryString;
    }

    public void setCategoryString(String categoryString) {
        this.categoryString = categoryString;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOpenComments() {
        return openComments;
    }

    public void setOpenComments(Integer openComments) {
        this.openComments = openComments;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getReposts() {
        return reposts;
    }

    public void setReposts(Integer reposts) {
        this.reposts = reposts;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Dislikes getDislikes() {
        return dislikes;
    }

    public void setDislikes(Dislikes dislikes) {
        this.dislikes = dislikes;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.postviews, flags);
        dest.writeString(this.text);
        dest.writeString(this.lang);
        dest.writeValue(this.authorWatch);
        dest.writeValue(this.status);
        dest.writeValue(this.adult);
        dest.writeValue(this.hidden);
        dest.writeValue(this.category);
        dest.writeValue(this.filter);
        dest.writeString(this.categoryString);
        dest.writeValue(this.ownerId);
        dest.writeValue(this.type);
        dest.writeValue(this.openComments);
        dest.writeValue(this.date);
        dest.writeValue(this.reposts);
        dest.writeParcelable(this.likes, flags);
        dest.writeParcelable(this.dislikes, flags);
        dest.writeParcelable(this.comments, flags);
        dest.writeList(this.tags);
        dest.writeList(this.attachments);
    }

    public Post() {
    }

    protected Post(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.postviews = in.readParcelable(Postviews.class.getClassLoader());
        this.text = in.readString();
        this.lang = in.readString();
        this.authorWatch = (Integer) in.readValue(Integer.class.getClassLoader());
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.adult = (Integer) in.readValue(Integer.class.getClassLoader());
        this.hidden = (Integer) in.readValue(Integer.class.getClassLoader());
        this.category = (Integer) in.readValue(Integer.class.getClassLoader());
        this.filter = (Integer) in.readValue(Integer.class.getClassLoader());
        this.categoryString = in.readString();
        this.ownerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.openComments = (Integer) in.readValue(Integer.class.getClassLoader());
        this.date = (Integer) in.readValue(Integer.class.getClassLoader());
        this.reposts = (Integer) in.readValue(Integer.class.getClassLoader());
        this.likes = in.readParcelable(Likes.class.getClassLoader());
        this.dislikes = in.readParcelable(Dislikes.class.getClassLoader());
        this.comments = in.readParcelable(Comments.class.getClassLoader());
        this.tags = new ArrayList<>();
        in.readList(this.tags, Object.class.getClassLoader());
        this.attachments = new ArrayList<Attachment>();
        in.readList(this.attachments, Attachment.class.getClassLoader());
    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}
