
package com.alexvereshchaga.feed.dataclasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size implements Parcelable {

    @SerializedName("photo_small")
    @Expose
    private PhotoSmall photoSmall;
    @SerializedName("photo_medium")
    @Expose
    private PhotoMedium photoMedium;
    @SerializedName("photo_big")
    @Expose
    private PhotoBig photoBig;

    public PhotoSmall getPhotoSmall() {
        return photoSmall;
    }

    public void setPhotoSmall(PhotoSmall photoSmall) {
        this.photoSmall = photoSmall;
    }

    public PhotoMedium getPhotoMedium() {
        return photoMedium;
    }

    public void setPhotoMedium(PhotoMedium photoMedium) {
        this.photoMedium = photoMedium;
    }

    public PhotoBig getPhotoBig() {
        return photoBig;
    }

    public void setPhotoBig(PhotoBig photoBig) {
        this.photoBig = photoBig;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.photoSmall, flags);
        dest.writeParcelable(this.photoMedium, flags);
        dest.writeParcelable(this.photoBig, flags);
    }

    public Size() {
    }

    protected Size(Parcel in) {
        this.photoSmall = in.readParcelable(PhotoSmall.class.getClassLoader());
        this.photoMedium = in.readParcelable(PhotoMedium.class.getClassLoader());
        this.photoBig = in.readParcelable(PhotoBig.class.getClassLoader());
    }

    public static final Parcelable.Creator<Size> CREATOR = new Parcelable.Creator<Size>() {
        @Override
        public Size createFromParcel(Parcel source) {
            return new Size(source);
        }

        @Override
        public Size[] newArray(int size) {
            return new Size[size];
        }
    };
}
