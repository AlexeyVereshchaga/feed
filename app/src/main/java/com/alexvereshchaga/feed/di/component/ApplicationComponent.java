package com.alexvereshchaga.feed.di.component;

import com.alexvereshchaga.feed.di.module.ApplicationModule;
import com.alexvereshchaga.feed.di.module.NetModule;
import com.alexvereshchaga.feed.managers.DataManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface ApplicationComponent {

    ConfigPersistentComponent configPersistentComponent();

    DataManager dataManager();

}
