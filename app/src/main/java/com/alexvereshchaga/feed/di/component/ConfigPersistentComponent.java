package com.alexvereshchaga.feed.di.component;

import com.alexvereshchaga.feed.di.module.ActivityModule;
import com.alexvereshchaga.feed.di.module.FragmentModule;
import com.alexvereshchaga.feed.di.module.ViewModule;
import com.alexvereshchaga.feed.di.scope.ConfigPersistent;

import dagger.Subcomponent;



@ConfigPersistent
@Subcomponent
public interface ConfigPersistentComponent {

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);

}
