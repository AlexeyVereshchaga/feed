package com.alexvereshchaga.feed.di.component;

import com.alexvereshchaga.feed.di.module.FragmentModule;
import com.alexvereshchaga.feed.di.module.PresenterModule;
import com.alexvereshchaga.feed.di.scope.PerFragment;
import com.alexvereshchaga.feed.main.feed.detail.view.PostDetailFragment;
import com.alexvereshchaga.feed.main.feed.list.view.FeedFragment;

import dagger.Subcomponent;


@PerFragment
@Subcomponent(modules = {FragmentModule.class, PresenterModule.class})
public interface FragmentComponent {

    void inject(FeedFragment feedFragment);

    void inject(PostDetailFragment postDetailFragment);

}
