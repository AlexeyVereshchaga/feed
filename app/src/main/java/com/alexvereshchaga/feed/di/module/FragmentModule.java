package com.alexvereshchaga.feed.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.alexvereshchaga.feed.di.qualifier.FragmentContext;
import com.alexvereshchaga.feed.di.scope.PerFragment;

import dagger.Module;
import dagger.Provides;


@Module
public class FragmentModule {

    private Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @PerFragment
    Fragment providesFragment() {
        return mFragment;
    }

    @Provides
    @PerFragment
    Activity provideActivity() {
        return mFragment.getActivity();
    }

    @Provides
    @PerFragment
    @FragmentContext
    Context providesContext() {
        return mFragment.getActivity();
    }

}
