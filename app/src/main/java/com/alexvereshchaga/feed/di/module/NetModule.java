package com.alexvereshchaga.feed.di.module;

import com.alexvereshchaga.feed.BuildConfig;
import com.alexvereshchaga.feed.app.Constants;
import com.alexvereshchaga.feed.rest.Urls;
import com.alexvereshchaga.feed.rest.error.ErrorHandlingCallAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    @Provides
    @Singleton
    @Named("main")
    OkHttpClient provideMainOkHttpClient() {
        HttpLoggingInterceptor netLogging = new HttpLoggingInterceptor();
        netLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(Constants.WRITE_TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .build();
                Response response = chain.proceed(request);
                return response;
            }
        });
//        builder.addNetworkInterceptor(logging);
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(netLogging);
        }
        return builder.build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .setPrettyPrinting()
                .setVersion(1.0)
                .create();
    }

    @Provides
    @Singleton
    @Named("main")
    Retrofit provideMainRetrofit(@Named("main") OkHttpClient okHttpClient, Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(ErrorHandlingCallAdapterFactory.create())
                .baseUrl(Urls.BASE_URL)
                .client(okHttpClient)
                .build();
        return retrofit;
    }
}
