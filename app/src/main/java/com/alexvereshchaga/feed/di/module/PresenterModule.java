package com.alexvereshchaga.feed.di.module;

import com.alexvereshchaga.feed.main.feed.detail.IPostDetailContract;
import com.alexvereshchaga.feed.main.feed.detail.presenter.PostDetailPresenter;
import com.alexvereshchaga.feed.main.feed.list.IFeedContract;
import com.alexvereshchaga.feed.main.feed.list.presenter.FeedPresenter;

import dagger.Binds;
import dagger.Module;



@Module
public abstract class PresenterModule {

    @Binds
    abstract IFeedContract.Presenter bindFeedPresenter(FeedPresenter feedPresenter);

    @Binds
    abstract IPostDetailContract.Presenter bindPostDetailsPresenter(PostDetailPresenter postDetailPresenter);


}
