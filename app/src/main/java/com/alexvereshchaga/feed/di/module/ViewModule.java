package com.alexvereshchaga.feed.di.module;

import android.content.Context;
import android.view.View;

import com.alexvereshchaga.feed.di.qualifier.ViewContext;
import com.alexvereshchaga.feed.di.scope.PerView;

import dagger.Module;
import dagger.Provides;


@Module
public class ViewModule {

    private final View mView;

    public ViewModule(View view){
        this.mView = view;
    }

    @Provides
    @PerView
    View provideView(){
        return mView;
    }

    @Provides
    @PerView
    @ViewContext
    Context provideContext(){
        return mView.getContext();
    }

}
