package com.alexvereshchaga.feed.eventbus;

import com.alexvereshchaga.feed.base.event.Event;
import com.alexvereshchaga.feed.base.event.EventFailRequest;
import com.alexvereshchaga.feed.base.event.EventFinishRequest;
import com.alexvereshchaga.feed.base.event.EventStartRequest;
import com.alexvereshchaga.feed.base.event.EventSuccessRequest;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;


@Singleton
public class EventBusController implements IEventSubject {

    private ArrayList<IEventObserver> mObservers;

    @Inject
    public EventBusController() {
        mObservers = new ArrayList<>();
    }

    @Override
    public void addObserver(IEventObserver iObserver) {
        if (!mObservers.contains(iObserver)) {
            mObservers.add(iObserver);
        }
    }

    @Override
    public void removeObserver(IEventObserver iObserver) {
        if (iObserver != null) {
            final int i = mObservers.indexOf(iObserver);
            if (i >= 0) {
                mObservers.remove(iObserver);
            }
        }
    }

    @Override
    public void removeAllObservers() {
        if (mObservers != null) {
            mObservers.clear();
        }
    }

    @Override
    public void notifyStartedWithAction(final int action) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventStartRequest(action));
        }
    }

    @Override
    public void notifyFinishWithAction(int action) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventFinishRequest(action));
        }
    }

    @Override
    public void notifySuccess(int action, Response response) {
        for (int i = 0; i < mObservers.size(); i++) {
            mObservers.get(i).onEvent(new EventSuccessRequest(action, response));
        }
    }

    @Override
    public void notifyFailed(int action, RetrofitException e, String message) {
        final int size = mObservers.size();
        for (int i = 0; i < size; i++) {
            mObservers.get(i).onEvent(new EventFailRequest(action, e, message));
        }
    }

    @Override
    public void notifyEvent(Event event) {
        final int size = mObservers.size();
        for (int i = 0; i < size; i++) {
            mObservers.get(i).onEvent(event);
        }
    }

    @Override
    public boolean containObserver(IEventObserver iObserver) {
        return mObservers.contains(iObserver);
    }

}
