package com.alexvereshchaga.feed.eventbus;


import com.alexvereshchaga.feed.base.event.Event;



public interface IEventObserver {

    void onEvent(Event event);

}
