package com.alexvereshchaga.feed.eventbus;

import com.alexvereshchaga.feed.base.event.Event;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

import retrofit2.Response;



public interface IEventSubject {

    void addObserver(final IEventObserver iObserver);

    void removeObserver(final IEventObserver iObserver);

    void removeAllObservers();

    void notifyStartedWithAction(final int action);

    void notifyFinishWithAction(final int action);

    void notifySuccess(final int actionCode, final Response response);

    void notifyFailed(final int actionCode, final RetrofitException e, final String message);

    boolean containObserver(final IEventObserver iObserver);

    void notifyEvent(final Event event);

}
