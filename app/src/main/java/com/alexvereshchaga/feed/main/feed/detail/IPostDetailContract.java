package com.alexvereshchaga.feed.main.feed.detail;

import com.alexvereshchaga.feed.base.mvp.IBaseMvpPresenter;
import com.alexvereshchaga.feed.base.mvp.IBaseProgressView;
import com.alexvereshchaga.feed.dataclasses.Post;

import java.util.List;

public interface IPostDetailContract {

    interface View extends IBaseProgressView {

        void initData(Post post);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

    }

}
