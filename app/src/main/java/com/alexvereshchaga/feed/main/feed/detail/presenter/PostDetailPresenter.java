package com.alexvereshchaga.feed.main.feed.detail.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.alexvereshchaga.feed.base.event.Event;
import com.alexvereshchaga.feed.base.mvp.BaseEventBusPresenter;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.di.scope.ConfigPersistent;
import com.alexvereshchaga.feed.main.feed.detail.IPostDetailContract;
import com.alexvereshchaga.feed.managers.RequestManager;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;


@ConfigPersistent
public class PostDetailPresenter extends BaseEventBusPresenter<IPostDetailContract.View> implements IPostDetailContract.Presenter {

    private static final String SAVE_ACCOUNT_DATA = "PostDetailPresenter.ACCOUNT_SAVE_ACCOUNT_DATA";

    private RequestManager mRequestManager;

    private Disposable mDisposable;

    private Post mPost;

    @Inject
    public PostDetailPresenter(RequestManager requestManager) {
        this.mRequestManager = requestManager;
        mDisposable = Disposables.empty();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initData(mPost);
        }
    }

    @Override
    public void setArguments(Object... params) {
        if (params != null) {
            mPost = (Post) params[0];
        }
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SAVE_ACCOUNT_DATA, mPost);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        mPost = savedInstanceState.getParcelable(SAVE_ACCOUNT_DATA);
    }

    @Override
    public void onEvent(Event event) {

    }
}
