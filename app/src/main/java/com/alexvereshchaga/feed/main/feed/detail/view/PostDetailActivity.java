package com.alexvereshchaga.feed.main.feed.detail.view;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.base.activity.BaseBackContainerActivity;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.utils.FieldConverter;

public class PostDetailActivity extends BaseBackContainerActivity {

    public static final String ACCOUNT_DATA = "PostDetailActivity.DATA";

    private Post mPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(FieldConverter.getString(R.string.detail_screen_title));
        getExtras();
        if(savedInstanceState == null) {
            openFragment();
        } else{
            onRestoreInstanceState(savedInstanceState);
        }
    }

    private void getExtras(){
        if(getIntent().getExtras() != null){
            mPost = getIntent().getExtras().getParcelable(ACCOUNT_DATA);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(ACCOUNT_DATA, mPost);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPost = savedInstanceState.getParcelable(ACCOUNT_DATA);
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, getScreenCreator()
                        .newInstance(PostDetailFragment.class, createBundle())).commitAllowingStateLoss();
    }

    private Bundle createBundle(){
        Bundle bundle = new Bundle();
        bundle.putParcelable(PostDetailFragment.ACCOUNT_DATA, mPost);
        return bundle;
    }

}
