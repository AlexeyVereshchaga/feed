package com.alexvereshchaga.feed.main.feed.detail.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.base.fragment.BaseMvpFragment;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.di.component.FragmentComponent;

import com.alexvereshchaga.feed.main.feed.detail.IPostDetailContract;
import com.alexvereshchaga.feed.view.progress.ProgressView;

import javax.inject.Inject;

import butterknife.BindView;

public class PostDetailFragment extends BaseMvpFragment implements IPostDetailContract.View {

    public static final String ACCOUNT_DATA = "PostDetailFragment.ACCOUNT_DATA";


    @BindView(R.id.tv_operation_type_title)
    TextView tvTitle;

    @Inject
    IPostDetailContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getArgs();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void getArgs() {
        if (getArguments() != null) {
            mPresenter.setArguments(getArguments().getParcelable(ACCOUNT_DATA));
        }
    }

    @Override
    public void initData(Post post) {
        if (post != null) {
            tvTitle.setText(post.getCategoryString());
        }
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return null;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_details;
    }

}
