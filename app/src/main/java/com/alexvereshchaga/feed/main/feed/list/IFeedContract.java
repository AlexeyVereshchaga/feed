package com.alexvereshchaga.feed.main.feed.list;

import com.alexvereshchaga.feed.base.mvp.IBaseMvpPresenter;
import com.alexvereshchaga.feed.base.mvp.IBaseProgressView;
import com.alexvereshchaga.feed.dataclasses.Post;

import java.util.ArrayList;

public interface IFeedContract {

    interface View extends IBaseProgressView {
        void initData();

        void updateData(ArrayList<Post> posts);

        void showDetailOperationScreen(Post post);
    }

    interface Presenter extends IBaseMvpPresenter<View> {
        void loadData(int page);

        void openDetailScreen(int position);
    }
}
