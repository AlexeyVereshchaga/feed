package com.alexvereshchaga.feed.main.feed.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.base.adapter.BaseLoadMoreAdapter;
import com.alexvereshchaga.feed.base.adapter.ILoadMoreListener;
import com.alexvereshchaga.feed.dataclasses.Post;

public class FeedAdapter extends BaseLoadMoreAdapter<Post> {

    private OnItemClickListener mOnItemClickListener;

    public FeedAdapter(Context context, ILoadMoreListener loadMoreListener) {
        super(context, loadMoreListener);
        mItemsInPage = 10;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    protected RecyclerView.ViewHolder createItem(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_operation_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    protected void bindItem(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            Post post = mDataList.get(position);
            ViewHolder currentViewHolder = (ViewHolder) holder;
            if (post != null) {

                if (post.getText() != null && !post.getText().isEmpty()) {
                    currentViewHolder.tvText.setVisibility(View.VISIBLE);
                    currentViewHolder.tvText.setText(post.getText());
                } else {
                    currentViewHolder.tvText.setVisibility(View.GONE);
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvText;
        View vDivider;

        public ViewHolder(View itemView) {
            super(itemView);
            tvText = itemView.findViewById(R.id.tv_text);
            vDivider = itemView.findViewById(R.id.v_divider);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
