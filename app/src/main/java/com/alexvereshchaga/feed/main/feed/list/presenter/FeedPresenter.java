package com.alexvereshchaga.feed.main.feed.list.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.alexvereshchaga.feed.base.event.Event;
import com.alexvereshchaga.feed.base.event.EventFailRequest;
import com.alexvereshchaga.feed.base.event.EventSuccessRequest;
import com.alexvereshchaga.feed.base.event.Wrapper;
import com.alexvereshchaga.feed.base.mvp.BaseEventBusPresenter;
import com.alexvereshchaga.feed.base.request.BaseRequestController;
import com.alexvereshchaga.feed.base.response.ErrorKind;
import com.alexvereshchaga.feed.base.response.IBaseResponseCallback;
import com.alexvereshchaga.feed.base.response.ResponseHandler;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.di.scope.ConfigPersistent;
import com.alexvereshchaga.feed.main.feed.list.IFeedContract;
import com.alexvereshchaga.feed.managers.RequestManager;
import com.alexvereshchaga.feed.rest.response.FeedResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import retrofit2.Response;

import static com.alexvereshchaga.feed.app.Constants.ACTION_CODE;

@ConfigPersistent
public class FeedPresenter extends BaseEventBusPresenter<IFeedContract.View> implements IFeedContract.Presenter, IBaseResponseCallback {

    private static final String SAVE_PAGE = "FeedPresenter.SAVE_PAGE";
    private static final String SAVE_FEED = "FeedPresenter.SAVE_FEED";

    private Disposable mDisposable;

    private RequestManager mRequestManager;

    private int mPage;
    private ArrayList<Post> posts;

    @Inject
    public FeedPresenter(RequestManager requestManager) {
        this.mRequestManager = requestManager;
        mDisposable = Disposables.empty();
        posts = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            getMvpView().initData();
        }
    }

    @Override
    public void setArguments(Object... params) {
    }

    @Override
    public void saveInstanceState(Bundle outState) {

        outState.putInt(SAVE_PAGE, mPage);
        outState.putParcelableArrayList(SAVE_FEED, posts);

    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {

        mPage = savedInstanceState.getInt(SAVE_PAGE);
        posts = savedInstanceState.getParcelableArrayList(SAVE_FEED);

    }

    @Override
    public void openDetailScreen(int position) {
        Post post = posts.get(position);
        if (post != null) {
            getMvpView().showDetailOperationScreen(post);
        }
    }

    @Override
    public void loadData(int page) {
        this.mPage = page;
        if (mPage == 1) {
            getMvpView().startLoading();
        }
        mDisposable = mRequestManager.getFeed(1, mPage, 10, new BaseRequestController(mEventBusController, ACTION_CODE));
        addDisposable(mDisposable);
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventType()) {
            case SUCCESS_REQUEST:
                switch (event.getActionCode()) {
                    case ACTION_CODE:
                        undisposable(mDisposable);
                        Response<FeedResponse> feedResponse = (Response<FeedResponse>) ((EventSuccessRequest) event).getData();
                        ResponseHandler.newInstance().handle(event.getActionCode(), feedResponse, this);
                        break;
                }
                break;
            case FAIL_REQUEST:
                switch (event.getActionCode()) {
                    case ACTION_CODE:
                        undisposable(mDisposable);
                        if (mPage == 1) {
                            getMvpView().errorLoading(((EventFailRequest) event).getThrowable());
                        }
                        break;
                }
                break;
        }

    }

    @Override
    public void onSuccess(int actionCode, int errorCode, Wrapper data) {
        switch (actionCode) {
            case ACTION_CODE:
                FeedResponse feedResponse = (FeedResponse) data.getValue();
                if (mPage == 1) {
                    getMvpView().completeLoading();
                    if (posts != null) {
                        posts.clear();
                    }
                }
                posts.addAll(feedResponse.getPosts());
                getMvpView().updateData(feedResponse.getPosts());
                break;
        }
    }

    @Override
    public void onError(ErrorKind errorKind, int actionCode, int errorCode, String errorMessage) {
        switch (errorKind) {
            case UNAUTHORIZED_ERROR:
                if (mPage == 1) {
                    getMvpView().completeLoading();
                }
                break;
            case BAD_REQUEST_ERROR:
                if (mPage == 1) {
                    getMvpView().errorLoading(mDataManager.getErrorMessage(errorCode, errorMessage));
                }
                break;
            case DEFAULT_ERROR:
                if (mPage == 1) {
                    getMvpView().errorLoading(mDataManager.getErrorMessage(errorCode, errorMessage));
                }
                break;
        }
    }
}
