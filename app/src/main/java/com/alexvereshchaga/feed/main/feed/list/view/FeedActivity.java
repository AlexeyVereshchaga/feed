package com.alexvereshchaga.feed.main.feed.list.view;

import android.os.Bundle;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.base.activity.BaseContainerActivity;

public class FeedActivity extends BaseContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Лента");
        if (savedInstanceState == null) {
            openFragment();
        }
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, getScreenCreator().newInstance(FeedFragment.class))
                .commitAllowingStateLoss();
    }
}