package com.alexvereshchaga.feed.main.feed.list.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.base.adapter.ILoadMoreListener;
import com.alexvereshchaga.feed.base.fragment.BaseMvpFragment;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.di.component.FragmentComponent;
import com.alexvereshchaga.feed.main.feed.detail.view.PostDetailActivity;
import com.alexvereshchaga.feed.main.feed.list.IFeedContract;
import com.alexvereshchaga.feed.main.feed.list.adapter.FeedAdapter;
import com.alexvereshchaga.feed.rest.error.RetrofitException;
import com.alexvereshchaga.feed.view.progress.ProgressView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;


public class FeedFragment extends BaseMvpFragment implements IFeedContract.View, ILoadMoreListener, FeedAdapter.OnItemClickListener {

    public static final String FEED = "FeedFragment.FEED";

    @BindView(R.id.rv_feed)
    RecyclerView rvFeed;
    @BindView(R.id.ll_feed_empty)
    LinearLayout llFeedEmpty;
    @BindView(R.id.ll_feed_content)
    LinearLayout llFeedContent;
    @BindView(R.id.progress_view)
    ProgressView progressView;
    @BindView(R.id.tv_emty_msg)
    TextView tvEmpryMsg;

    @Inject
    IFeedContract.Presenter mPresenter;

    private FeedAdapter feedAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgs();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initAdapters();
        mPresenter.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPresenter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void getArgs() {
        if (getArguments() != null) {
            mPresenter.setArguments(getArguments().getString(FEED));
        }
    }

    private void initAdapters() {
        feedAdapter = new FeedAdapter(mActivity, this);
        feedAdapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvFeed.setLayoutManager(layoutManager);
        rvFeed.setAdapter(feedAdapter);
    }

    @Override
    public void loadData(int currentPage) {
        mPresenter.loadData(currentPage);
    }

    @Override
    public void initData() {
        feedAdapter.loadData();
    }

    @Override
    public void updateData(ArrayList<Post> posts) {
        if (posts == null || posts.size() == 0) {
            if (feedAdapter.getItemCount() == 0) {
                rvFeed.setVisibility(View.GONE);
                llFeedEmpty.setVisibility(View.VISIBLE);
            }
        } else {
            llFeedEmpty.setVisibility(View.GONE);
            rvFeed.setVisibility(View.VISIBLE);
            feedAdapter.updateData(posts);
        }
    }

    @Override
    public void onItemClick(int position) {
        mPresenter.openDetailScreen(position);
    }

    @Override
    public void showDetailOperationScreen(Post post) {
        getScreenCreator().startActivity(this, mActivity, PostDetailActivity.class, getOperationDetailBundle(post));
    }

    private Bundle getOperationDetailBundle(Post post) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PostDetailActivity.ACCOUNT_DATA, post);
        return bundle;
    }

    @Override
    protected void retry() {
        feedAdapter.refresh();
    }

    @Override
    public void startLoading() {
        super.startLoading();
        llFeedContent.setVisibility(View.GONE);

    }

    @Override
    public void completeLoading() {
        super.completeLoading();
        llFeedContent.setVisibility(View.VISIBLE);

    }

    @Override
    public void errorLoading(ErrorData errorData) {
        super.errorLoading(errorData);
        llFeedContent.setVisibility(View.GONE);

    }

    @Override
    public void errorLoading(RetrofitException retrofitException) {
        super.errorLoading(retrofitException);
        llFeedContent.setVisibility(View.GONE);

    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    protected ProgressView getProgressView() {
        return progressView;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_feed;
    }
}
