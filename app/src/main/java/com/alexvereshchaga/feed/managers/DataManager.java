package com.alexvereshchaga.feed.managers;

import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.dataclasses.Post;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class DataManager {

    private ErrorMessageManager mErrorMessageManager;
    private DatabaseManager mDatabaseManager;

    @Inject
    public DataManager(ErrorMessageManager errorMessageManager, DatabaseManager databaseManager) {
        this.mErrorMessageManager = errorMessageManager;
                this.mDatabaseManager = databaseManager;
    }

    public ErrorData getErrorMessage(int errorCode, String errorMessage) {
        return mErrorMessageManager.getErrorMessage(errorCode, errorMessage);
    }

    public Single<List<Post>> getAllPosts() {
        return mDatabaseManager.getAllPosts();
    }

    public Single<List<Post>> getProvidersByQuery(String query) {
        return mDatabaseManager.getPostsByQuery(query);
    }

    //search provider by id
    public Single<Post> getProvidersById(String pid) {
        return mDatabaseManager.getPostById(pid);
    }

 }
