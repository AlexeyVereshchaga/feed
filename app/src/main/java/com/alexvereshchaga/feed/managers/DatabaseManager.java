package com.alexvereshchaga.feed.managers;

import android.content.Context;

import com.alexvereshchaga.feed.database.AppDatabase;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.alexvereshchaga.feed.di.qualifier.ApplicationContext;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class DatabaseManager {

    private AppDatabase mAppDatabase;

    @Inject
    public DatabaseManager(@ApplicationContext Context context) {
        mAppDatabase = AppDatabase.getInstance(context);
    }

    public Single<List<Post>> getAllPosts() {
        return mAppDatabase.postDao().getAllPosts();
    }

    public void insertOrReplacePost(Post post) {
        mAppDatabase.postDao().insertPost(post);
    }

    public void insertOrReplacePosts(List<Post> providers) {
        mAppDatabase.postDao().insertPosts(providers);
    }

    public void deleteAllProviders() {
        mAppDatabase.postDao().deleteAllPosts();
    }


    public Single<List<Post>> getPostsByQuery(String query) {
        return mAppDatabase.postDao().getPostsByQuery("%" + query + "%");
    }

    public Single<Post> getPostById(String pid) {
        return mAppDatabase.postDao().getPostById(pid);
    }
}
