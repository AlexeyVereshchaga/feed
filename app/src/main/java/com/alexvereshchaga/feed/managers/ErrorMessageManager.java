package com.alexvereshchaga.feed.managers;

import android.content.res.TypedArray;
import android.text.TextUtils;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.utils.FieldConverter;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ErrorMessageManager {

    private HashMap<Integer, ErrorData> mErrorMap;

    @Inject
    public ErrorMessageManager() {
        mErrorMap = getErrorMap();
    }

    public ErrorData getErrorMessage(int errorCode, String errorMessage) {
        if (mErrorMap == null) {
            mErrorMap = getErrorMap();
        }
        if (mErrorMap.get(errorCode) == null) {
            ErrorData errorData = new ErrorData(errorCode,
                    TextUtils.isEmpty(errorMessage)
                            ? FieldConverter.getString(R.string.message_error_default)
                            : errorMessage,
                    R.drawable.ic_error_msg);
            return errorData;
        } else {
            ErrorData errorData;
            if (TextUtils.isEmpty(errorMessage)) {
                errorData = mErrorMap.get(errorCode);
            } else {
                errorData = mErrorMap.get(errorCode);
                errorData.setErrorMessage(errorMessage);
            }
            return errorData;
        }
    }

    private HashMap<Integer, ErrorData> getErrorMap() {
        int[] errorCodeArray = FieldConverter.getIntArray(R.array.error_code);
        TypedArray errorIcon = FieldConverter.getTypedArray(R.array.error_icon);
        String[] errorMessageArray = FieldConverter.getStringArray(R.array.error_message);
        HashMap<Integer, ErrorData> errorMap = new HashMap<>();
        for (int i = 0; i < errorCodeArray.length; i++) {
            ErrorData errorData = new ErrorData(errorCodeArray[i], errorMessageArray[i], errorIcon.getResourceId(i, -1));
            errorMap.put(errorCodeArray[i], errorData);
        }
        return errorMap;
    }

}
