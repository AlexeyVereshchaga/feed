package com.alexvereshchaga.feed.managers;

import com.alexvereshchaga.feed.base.request.IRequestCallback;
import com.alexvereshchaga.feed.rest.error.ErrorConsumer;
import com.alexvereshchaga.feed.rest.error.RetrofitException;
import com.alexvereshchaga.feed.rest.response.FeedResponse;
import com.alexvereshchaga.feed.rest.service.UserProductService;

import java.util.LinkedHashMap;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.Retrofit;

@Singleton
public class RequestManager {

    private Retrofit mMainRetrofit;
    private DatabaseManager mDatabaseManager;

    @Inject
    public RequestManager(@Named("main") Retrofit mMainRetrofit, DatabaseManager mDatabaseManager) {
        this.mMainRetrofit = mMainRetrofit;
        this.mDatabaseManager = mDatabaseManager;
    }

    private <S> S createService(Retrofit retrofit, Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public <T extends Response> Disposable makeSingleRequest(Single<T> single, IRequestCallback requestCallback) {
        return single.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnSuccess(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeObservableRequest(Observable<T> observable, IRequestCallback requestCallback) {
        return observable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnNext(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeFlowableRequest(Flowable<T> flowable, IRequestCallback requestCallback) {
        return flowable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnNext(s -> requestCallback.onSuccessResponse(s))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Disposable makeMaybeRequest(Maybe<T> maybe, IRequestCallback requestCallback) {
        return maybe.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnComplete(() -> requestCallback.onSuccessResponse(null))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public Disposable makeCompletableRequest(Completable completable, IRequestCallback requestCallback) {
        return completable.doOnSubscribe(d -> requestCallback.onStartRequest())
                .doOnComplete(() -> requestCallback.onSuccessResponse(null))
                .doOnError(ErrorConsumer.consume(new ErrorConsumer.OnErrorHandler() {
                    @Override
                    public void onHandleError(RetrofitException t, String message) {
                        requestCallback.onErrorResponse(t, message);
                    }
                }))
                .doFinally(() -> requestCallback.onFinishRequest())
                .subscribe();
    }

    public <T extends Response> Single<T> makeSingleIOMainSubscriber(Single<T> single) {
        return single.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Observable<T> makeObservableIOMainSubscriber(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Flowable<T> makeFlowableIOMainSubscriber(Flowable<T> flowable) {
        return flowable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T extends Response> Maybe<T> makeMaybeIOMainSubscriber(Maybe<T> maybe) {
        return maybe.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Completable makeCompletableIOMainSubscriber(Completable completable) {
        return completable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<FeedResponse>> getFeed(Integer type, Integer count, Integer offset) {
        return createService(mMainRetrofit, UserProductService.class).getFeed(type, count, offset);
    }

    public Disposable getFeed(Integer type, Integer count, Integer offset, IRequestCallback requestCallback) {
        return makeSingleRequest(makeSingleIOMainSubscriber(getFeed(type, count, offset)), requestCallback);
    }
}
