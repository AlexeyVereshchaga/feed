package com.alexvereshchaga.feed.progressdialog;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

public class ProgressDialogManager implements ProgressDialog.OnProgressDialogVisibleListener {

    private static ProgressDialogManager ourInstance = new ProgressDialogManager();

    private ProgressDialog mProgressDialog;

    private ProgressEvent mProgressEvent;

    private ProgressDialogManager() {
    }

    public static ProgressDialogManager getInstance() {
        return ourInstance;
    }

    private void subscribe() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog();
            mProgressDialog.setOnProgressDialogVisibleListener(this);
        }
    }

    public void unsubscribe() {
        if (mProgressDialog != null && mProgressDialog.isAdded()) {
            mProgressDialog.dismissAllowingStateLoss();
            mProgressDialog = null;
        }
    }

    public void startLoading(AppCompatActivity activity) {
        subscribe();
        if (!mProgressDialog.isVisible() && !mProgressDialog.isAdded()) {
            mProgressDialog.show(activity.getSupportFragmentManager(), ProgressDialog.TAG);
        }
        mProgressEvent = new ProgressEvent(ProgressEvent.START_PROGRESS);
        mProgressDialog.startLoading();
    }

    public void startLoading(Fragment fragment) {
        subscribe();
        try {
            if (!mProgressDialog.isVisible() && !mProgressDialog.isAdded()) {
                mProgressDialog.show(fragment.getFragmentManager(), ProgressDialog.TAG);
                mProgressEvent = new ProgressEvent(ProgressEvent.START_PROGRESS);
                mProgressDialog.startLoading();
            }
        } catch (Exception e) {
            Log.e("ProgressDialogManager", "startLoading", e);
        }
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData) {
        if (mProgressDialog != null && mProgressDialog.isAdded()) {
            mProgressDialog.errorLoading(onOkButtonClickCallbackListener, errorData);
        } else {
            mProgressEvent = new ProgressEvent(ProgressEvent.ERROR_PROGRESS, errorData, onOkButtonClickCallbackListener);
        }
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException) {
        if (mProgressDialog != null && mProgressDialog.isAdded()) {
            mProgressDialog.errorLoading(onOkButtonClickCallbackListener, retrofitException);
        } else {
            mProgressEvent = new ProgressEvent(ProgressEvent.ERROR_PROGRESS, retrofitException, onOkButtonClickCallbackListener);
        }
    }

    public void completeLoading() {
        if (mProgressDialog != null && mProgressDialog.isAdded()) {
            mProgressDialog.completeLoading();
            unsubscribe();
        } else {
            mProgressEvent = new ProgressEvent(ProgressEvent.COMPLETE_PROGRESS);
        }
    }

    @Override
    public void onProgressDialogVisible() {
        switch (mProgressEvent.getStatus()) {
            case ProgressEvent.COMPLETE_PROGRESS:
                completeLoading();
                break;
            case ProgressEvent.ERROR_PROGRESS:
                if (mProgressEvent.getRetrofitException() != null) {
                    errorLoading(mProgressEvent.getOnOkButtonClickCallbackListener(), mProgressEvent.getRetrofitException());
                } else {
                    errorLoading(mProgressEvent.getOnOkButtonClickCallbackListener(), mProgressEvent.getErrorData());
                }
                break;
        }
    }
}
