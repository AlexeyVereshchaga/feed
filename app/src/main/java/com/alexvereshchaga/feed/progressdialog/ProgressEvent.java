package com.alexvereshchaga.feed.progressdialog;


import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

public class ProgressEvent {

    public static final int START_PROGRESS = 0;
    public static final int COMPLETE_PROGRESS = 1;
    public static final int ERROR_PROGRESS = 2;

    private int mStatus;
    private RetrofitException mRetrofitException;
    private ErrorData mErrorData;
    private ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener;

    public ProgressEvent(int mStatus) {
        this.mStatus = mStatus;
    }

    public ProgressEvent(int mStatus, RetrofitException mRetrofitException) {
        this.mStatus = mStatus;
        this.mRetrofitException = mRetrofitException;
    }

    public ProgressEvent(int mStatus, ErrorData mErrorData) {
        this.mStatus = mStatus;
        this.mErrorData = mErrorData;
    }

    public ProgressEvent(int mStatus, RetrofitException mRetrofitException, ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener) {
        this.mStatus = mStatus;
        this.mRetrofitException = mRetrofitException;
        this.mOnOkButtonClickCallbackListener = mOnOkButtonClickCallbackListener;
    }

    public ProgressEvent(int mStatus, ErrorData mErrorData, ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener) {
        this.mStatus = mStatus;
        this.mErrorData = mErrorData;
        this.mOnOkButtonClickCallbackListener = mOnOkButtonClickCallbackListener;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public RetrofitException getRetrofitException() {
        return mRetrofitException;
    }

    public void setRetrofitException(RetrofitException mRetrofitException) {
        this.mRetrofitException = mRetrofitException;
    }

    public ErrorData getErrorData() {
        return mErrorData;
    }

    public void setErrorData(ErrorData mErrorData) {
        this.mErrorData = mErrorData;
    }

    public ProgressDialog.OnOkButtonClickCallbackListener getOnOkButtonClickCallbackListener() {
        return mOnOkButtonClickCallbackListener;
    }

    public void setOnOkButtonClickCallbackListener(ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener) {
        this.mOnOkButtonClickCallbackListener = mOnOkButtonClickCallbackListener;
    }
}
