package com.alexvereshchaga.feed.progressdialog.custom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.app.Constants;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.progressdialog.ProgressDialog;
import com.alexvereshchaga.feed.progressdialog.ProgressEvent;
import com.alexvereshchaga.feed.progressdialog.custom.observer.ProgressObservable;
import com.alexvereshchaga.feed.progressdialog.custom.observer.ProgressObserver;
import com.alexvereshchaga.feed.rest.error.RetrofitException;
import com.alexvereshchaga.feed.utils.UI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ProgressDialogActivity extends AppCompatActivity implements ProgressObserver {

    public static final String TAG = "ProgressDialog.TAG";

    @BindView(R.id.rl_dialog_progress)
    RelativeLayout rlProgress;
    @BindView(R.id.pb_dialog_load)
    ProgressBar pbLoad;
    @BindView(R.id.ll_dialog_progress_error)
    LinearLayout llError;
    @BindView(R.id.iv_error_logo)
    ImageView ivErrorLogo;
    @BindView(R.id.tv_dialog_progress_error)
    TextView tvError;
    @BindView(R.id.btn_dialog_progress_settings)
    TextView btnDialogProgressSettings;

    private Unbinder mUnbinder;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, ProgressDialogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.startActivity(intent);
        UI.openActivityWithoutAnimation(activity);
    }

    public static void startActivity(Fragment fragment, Activity activity) {
        Intent intent = new Intent(activity, ProgressDialogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        fragment.startActivity(intent);
        UI.openActivityWithoutAnimation(activity);
    }

    private ProgressDialog.OnProgressDialogVisibleListener mListener;

    private ErrorData mErrorData;
    private RetrofitException mRetrofitException;

    private ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        mUnbinder = ButterKnife.bind(this);
        ProgressObservable.newInstance().registerObserver(this);
        setFinishOnTouchOutside(false);
    }

    public void setOnOkButtonClickCallbackListener(ProgressDialog.OnOkButtonClickCallbackListener mOnOkButtonClickCallbackListener) {
        this.mOnOkButtonClickCallbackListener = mOnOkButtonClickCallbackListener;
    }

    @OnClick({R.id.btn_dialog_progress_settings, R.id.btn_dialog_progress_close})
    void okClick(View view) {
        switch (view.getId()) {
            case R.id.btn_dialog_progress_settings:
                Intent settingsIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                startActivityForResult(settingsIntent, Constants.START_SETTINGS);
                break;
            case R.id.btn_dialog_progress_close:
                if (mOnOkButtonClickCallbackListener != null) {
                    if (mErrorData != null) {
                        mOnOkButtonClickCallbackListener.onOkButtonClick(mErrorData.getErrorCode());
                    }
                }
                break;
        }
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void event(ProgressEvent progressEvent) {
        switch (progressEvent.getStatus()){
            case ProgressEvent.COMPLETE_PROGRESS:
                completeLoading();
                break;
            case ProgressEvent.ERROR_PROGRESS:
                if(progressEvent.getRetrofitException() != null) {
                    errorLoading(progressEvent.getOnOkButtonClickCallbackListener(), progressEvent.getRetrofitException());
                } else{
                    errorLoading(progressEvent.getOnOkButtonClickCallbackListener(), progressEvent.getErrorData());
                }
                break;
        }
    }

    public void completeLoading() {
        setFinishOnTouchOutside(true);
        rlProgress.setVisibility(View.GONE);
        llError.setVisibility(View.GONE);
        pbLoad.setVisibility(View.GONE);
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, ErrorData errorData) {
        setFinishOnTouchOutside(true);
        this.mOnOkButtonClickCallbackListener = onOkButtonClickCallbackListener;
        this.mErrorData = errorData;
        btnDialogProgressSettings.setVisibility(View.GONE);
        if(mErrorData != null) {
            tvError.setText(mErrorData.getErrorMessage());
        }
        ivErrorLogo.setImageResource(errorData.getErrorDrawable());
        rlProgress.setVisibility(View.VISIBLE);
        llError.setVisibility(View.VISIBLE);
        pbLoad.setVisibility(View.GONE);
    }

    public void errorLoading(ProgressDialog.OnOkButtonClickCallbackListener onOkButtonClickCallbackListener, RetrofitException retrofitException) {
        setFinishOnTouchOutside(true);
        this.mOnOkButtonClickCallbackListener = onOkButtonClickCallbackListener;
        this.mRetrofitException = retrofitException;
        if (retrofitException.getKind() == RetrofitException.ERROR_NETWORK || retrofitException.getKind() == RetrofitException.ERROR_SOCKET_TIMEOUT) {
            btnDialogProgressSettings.setVisibility(View.VISIBLE);
        } else {
            btnDialogProgressSettings.setVisibility(View.GONE);
        }
        if(retrofitException != null) {
            tvError.setText(retrofitException.getMessage());
        }
        ivErrorLogo.setImageResource(retrofitException.getErrorIcon());
        rlProgress.setVisibility(View.VISIBLE);
        llError.setVisibility(View.VISIBLE);
        pbLoad.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        return;
    }

    public interface OnOkButtonClickCallbackListener {
        void onOkButtonClick(int errorCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ProgressObservable.newInstance().unRegisterObserver(this);
        mUnbinder.unbind();
    }


}
