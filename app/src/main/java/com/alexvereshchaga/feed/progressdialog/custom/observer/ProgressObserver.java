package com.alexvereshchaga.feed.progressdialog.custom.observer;

import com.alexvereshchaga.feed.progressdialog.ProgressEvent;


public interface ProgressObserver {

    void event(ProgressEvent progressEvent);

}
