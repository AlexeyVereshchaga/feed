package com.alexvereshchaga.feed.progressdialog.custom.observer;

import com.alexvereshchaga.feed.progressdialog.ProgressEvent;

public interface ProgressSubject {

    public void registerObserver(ProgressObserver observer);

    public void notifyObservers(ProgressEvent progressEvent);

    public void unRegisterObserver(ProgressObserver observer);

}
