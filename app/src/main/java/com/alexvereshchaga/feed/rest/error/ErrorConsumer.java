package com.alexvereshchaga.feed.rest.error;

import android.util.Log;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

public class ErrorConsumer implements Consumer<Throwable> {

    static {
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {

            }
        });
    }

    public static ErrorConsumer consume(OnErrorHandler errorHandler) {
        return new ErrorConsumer(errorHandler);
    }

    private final OnErrorHandler onErrorHandler;

    private ErrorConsumer(@NonNull OnErrorHandler errorHandler) {
        this.onErrorHandler = errorHandler;
    }

    @Override
    public final void accept(Throwable throwable) {
        if (throwable instanceof RetrofitException) {
            RetrofitException re = (RetrofitException) throwable;
            if (onErrorHandler != null) {
                onErrorHandler.onHandleError(re, re.getMessage());
            }
        } else {
            Log.e("ErrorConsumer", "accept: ", throwable);
        }
    }

    public interface OnErrorHandler {
        void onHandleError(RetrofitException t, String message);
    }
}
