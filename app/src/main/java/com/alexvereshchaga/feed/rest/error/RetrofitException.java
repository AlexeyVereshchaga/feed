package com.alexvereshchaga.feed.rest.error;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.utils.FieldConverter;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RetrofitException  extends RuntimeException {

    private static final String MESSAGE = "mMessage";

    /**
     * An {@link IOException} occurred while communicating to the server.
     */
    public static final int ERROR_NETWORK = 100001;

    /**
     * A socket timeout exception
     */
    public static final int ERROR_SOCKET_TIMEOUT = 100002;

    /**
     * A non-200 HTTP status code was received from the server.
     */
    public static final int ERROR_HTTP = 100003;

    /**
     * An internal error occurred while attempting to execute a request. It is best practice to
     * re-throw this exception so your application crashes.
     */
    public static final int ERROR_UNEXPECTED = 100004;

    static RetrofitException httpError(Throwable throwable, Response response, Retrofit retrofit) {
        String message = null;
        if(response != null) {
            if (response.message() != null) {
                message = response.code() + " " + response.message();
            } else {
                message = FieldConverter.getString((R.string.message_error_default));
            }
        } else{
            message = FieldConverter.getString((R.string.message_error_default));
        }
        return new RetrofitException(message, response, ERROR_HTTP, throwable, retrofit, android.R.drawable.ic_delete);
    }

    public static RetrofitException socketTimeoutError(Throwable throwable) {
        return new RetrofitException(FieldConverter.getString(R.string.message_error_socket), null, ERROR_SOCKET_TIMEOUT, throwable, null, android.R.drawable.ic_delete);
    }

    public static RetrofitException networkError(Throwable throwable) {
        return new RetrofitException(FieldConverter.getString(R.string.message_error_connect), null, ERROR_NETWORK, throwable, null, R.drawable.ic_internet_connect_error);
    }

    public static RetrofitException unexpectedError(Throwable throwable) {
        return new RetrofitException(FieldConverter.getString(R.string.message_error_default), null, ERROR_UNEXPECTED, throwable, null, android.R.drawable.ic_delete);
    }

    private final Response response;
    private final int kind;
    private final String message;
    private final Retrofit retrofit;
    private int errorIcon;

    private RetrofitException(String message, Response response, int kind, Throwable exception, Retrofit retrofit, int errorIcon) {
        super(message, exception);
        this.message = message;
        this.response = response;
        this.kind = kind;
        this.retrofit = retrofit;
        this.errorIcon = errorIcon;
    }

    /**
     * Message error.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Response object containing status code, headers, body, etc.
     */
    public Response getResponse() {
        return response;
    }

    /**
     * The event mKind which triggered this error.
     */
    public int getKind() {
        return kind;
    }

    /**
     * The Retrofit this request was executed on
     */
    public Retrofit getRetrofit() {
        return retrofit;
    }

    /**
     * Icon error
     */
    public int getErrorIcon() {
        return errorIcon;
    }

    /**
     * HTTP mResponse body converted to specified {@code type}. {@code null} if there is no
     * mResponse.
     *
     * @throws IOException if unable to convert the body to the specified {@code type}.
     */
    public <T> T getErrorBodyAs(Class<T> type) throws IOException {
        if (response == null || response.errorBody() == null) {
            return null;
        }
        Converter<ResponseBody, T> converter = retrofit.responseBodyConverter(type, new Annotation[0]);
        return converter.convert(response.errorBody());
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return (message != null) ? message : s;
    }
}
