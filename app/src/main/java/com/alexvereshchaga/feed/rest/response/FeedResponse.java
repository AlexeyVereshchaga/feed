package com.alexvereshchaga.feed.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.alexvereshchaga.feed.base.response.BaseResponse;
import com.alexvereshchaga.feed.dataclasses.Post;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FeedResponse extends BaseResponse {

    @SerializedName("data")
    ArrayList<Post> posts;

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.posts);
    }

    public FeedResponse() {
    }

    protected FeedResponse(Parcel in) {
        super(in);
        this.posts = in.createTypedArrayList(Post.CREATOR);
    }

    public static final Parcelable.Creator<FeedResponse> CREATOR = new Parcelable.Creator<FeedResponse>() {
        @Override
        public FeedResponse createFromParcel(Parcel source) {
            return new FeedResponse(source);
        }

        @Override
        public FeedResponse[] newArray(int size) {
            return new FeedResponse[size];
        }
    };
}
