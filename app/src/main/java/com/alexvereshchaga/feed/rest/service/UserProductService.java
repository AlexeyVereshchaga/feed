package com.alexvereshchaga.feed.rest.service;

import com.alexvereshchaga.feed.rest.Urls;
import com.alexvereshchaga.feed.rest.response.FeedResponse;


import java.util.LinkedHashMap;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface UserProductService {

    @POST(Urls.FEED_PATH)
    Single<Response<FeedResponse>> getFeed(@Query("type") Integer type,
                                           @Query("count") Integer count,
                                           @Query("offset") Integer offset);

}
