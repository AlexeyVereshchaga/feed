package com.alexvereshchaga.feed.utils;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.alexvereshchaga.feed.app.App;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class FieldConverter {

    public static String getString(int resId) {
        return App.mInstance.getResources().getString(resId);
    }

    public static String[] getStringArray(int resId) {
        return App.mInstance.getResources().getStringArray(resId);
    }

    public static int[] getIntArray(int resId) {
        return App.mInstance.getResources().getIntArray(resId);
    }

    public static TypedArray getTypedArray(int resId) {
        return App.mInstance.getResources().obtainTypedArray(resId);
    }
}