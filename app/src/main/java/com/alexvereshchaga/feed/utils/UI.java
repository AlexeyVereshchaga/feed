package com.alexvereshchaga.feed.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.ViewGroup;

import com.alexvereshchaga.feed.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;



public class UI {

    public static void animationOpenActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    public static void animationCloseActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    public static void openActivityWithoutAnimation(Activity activity) {
        activity.overridePendingTransition(0, 0);
    }



    public static void crossFade(ViewGroup showLayout, ViewGroup hideLayout) {
        hideLayout.animate()
                .alpha(0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        hideLayout.setVisibility(GONE);
                        showLayout.setAlpha(0f);
                        showLayout.setVisibility(VISIBLE);
                        showLayout.animate()
                                .alpha(1f)
                                .setDuration(300)
                                .setListener(null);
                    }
                });
    }

    interface IProductViewCallback{
        void showBalanceDialog();
    }


}
