package com.alexvereshchaga.feed.view.progress;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

public class ProgressView extends RelativeLayout {

    private ProgressBar mPbLoad;
    private TextView mTvProgressError;
    private ImageView mIvErrorIcon;
    private Button mBtnProgressErrorAction;
    private Button mBtnSettings;
    private LinearLayout mLlProgressError;
    private RelativeLayout mRlProgress;

    private OnRetryListener mOnRetryListener;

    private ErrorData mErrorData;
    private RetrofitException mRetrofitException;

    public void setOnRetryListener(OnRetryListener onRetryListener) {
        this.mOnRetryListener = onRetryListener;
    }

    public ProgressView(Context context) {
        super(context);
        init(context);
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.include_progress_layout, this);
    }

    public void startLoading() {
        this.setVisibility(VISIBLE);
        mRlProgress.setVisibility(View.VISIBLE);
        mPbLoad.setVisibility(View.VISIBLE);
        mLlProgressError.setVisibility(View.GONE);
    }

    public void completeLoading() {
        this.setVisibility(GONE);
        mRlProgress.setVisibility(View.GONE);
        mPbLoad.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.GONE);
    }

    public void errorLoading(ErrorData errorData) {
        this.setVisibility(VISIBLE);
        this.mErrorData = errorData;
        mRlProgress.setVisibility(View.VISIBLE);
        mPbLoad.setVisibility(View.GONE);
        mBtnSettings.setVisibility(View.GONE);
        mIvErrorIcon.setImageResource(errorData.getErrorDrawable());
        mLlProgressError.setVisibility(View.VISIBLE);
        if (mErrorData != null) {
            mTvProgressError.setText(mErrorData.getErrorMessage());
        }
    }

    public void errorLoading(RetrofitException retrofitException) {
        this.setVisibility(VISIBLE);
        this.mRetrofitException = retrofitException;
        mRlProgress.setVisibility(View.VISIBLE);
        mPbLoad.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.VISIBLE);
        if (retrofitException.getKind() == RetrofitException.ERROR_NETWORK || retrofitException.getKind() == RetrofitException.ERROR_SOCKET_TIMEOUT) {
            mBtnSettings.setVisibility(View.VISIBLE);
        } else {
            mBtnSettings.setVisibility(View.GONE);
        }
        mIvErrorIcon.setImageResource(retrofitException.getErrorIcon());
        if (mRetrofitException != null) {
            mTvProgressError.setText(mRetrofitException.getMessage());
        }
    }

    public void gone() {
        this.setVisibility(GONE);
        mRlProgress.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.GONE);
        mPbLoad.setVisibility(View.GONE);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mPbLoad = (ProgressBar) this.findViewById(R.id.pb_load);
        mIvErrorIcon = (ImageView) this.findViewById(R.id.iv_error_icon);
        mTvProgressError = (TextView) this.findViewById(R.id.tv_progress_error);
        mBtnProgressErrorAction = (Button) this.findViewById(R.id.btn_progress_error_retry);
        mBtnSettings = (Button) this.findViewById(R.id.btn_progress_error_settings);
        mLlProgressError = (LinearLayout) this.findViewById(R.id.ll_progress_error);
        mRlProgress = (RelativeLayout) this.findViewById(R.id.rl_progress);

        mBtnProgressErrorAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnRetryListener != null) {
                    mOnRetryListener.onRetry();
                }
            }
        });
        mBtnSettings.setOnClickListener(view -> {
            Intent settingsIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
            getContext().startActivity(settingsIntent);
        });
    }

    public interface OnRetryListener {
        void onRetry();
    }

}
