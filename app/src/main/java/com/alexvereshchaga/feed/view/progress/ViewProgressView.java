package com.alexvereshchaga.feed.view.progress;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexvereshchaga.feed.R;
import com.alexvereshchaga.feed.dataclasses.ErrorData;
import com.alexvereshchaga.feed.rest.error.RetrofitException;

public class ViewProgressView extends RelativeLayout {

    private ProgressBar mPbLoad;
    private TextView mTvProgressError;
    private LinearLayout mLlProgressError;
    private TextView mTvProgressErrorAction;

    private OnRetryListener mOnRetryListener;

    private ErrorData mErrorData;
    private RetrofitException mRetrofitException;

    public void setOnRetryListener(OnRetryListener onRetryListener){
        this.mOnRetryListener = onRetryListener;
    }

    public ViewProgressView(Context context) {
        super(context);
        init(context);
    }

    public ViewProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ViewProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.include_view_progress_layout, this);
    }

    public void startLoading() {
        this.setVisibility(VISIBLE);
        mPbLoad.setVisibility(View.VISIBLE);
        mLlProgressError.setVisibility(View.GONE);
    }

    public void completeLoading() {
        this.setVisibility(GONE);
        mPbLoad.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.GONE);
    }

    public void errorLoading(ErrorData errorData) {
        this.setVisibility(VISIBLE);
        this.mErrorData = errorData;
        mPbLoad.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.VISIBLE);
        if(mErrorData != null) {
            mTvProgressError.setText(mErrorData.getErrorMessage());
        }
    }

    public void errorLoading(RetrofitException retrofitException) {
        this.setVisibility(VISIBLE);
        this.mRetrofitException = retrofitException;
        mPbLoad.setVisibility(View.GONE);
        mLlProgressError.setVisibility(View.VISIBLE);
        if(mRetrofitException != null) {
            mTvProgressError.setText(mRetrofitException.getMessage());
        }
    }

    public void gone(){
        this.setVisibility(GONE);
        mLlProgressError.setVisibility(View.GONE);
        mPbLoad.setVisibility(View.GONE);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mPbLoad = (ProgressBar) this.findViewById(R.id.pb_load);
        mTvProgressError = (TextView) this.findViewById(R.id.tv_progress_error);
        mLlProgressError = (LinearLayout) this.findViewById(R.id.ll_progress_error);
        mTvProgressErrorAction = (TextView) this.findViewById(R.id.btn_progress_error_retry);

        mTvProgressErrorAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnRetryListener != null){
                    mOnRetryListener.onRetry();
                }
            }
        });
    }

    public interface OnRetryListener{
        void onRetry();
    }
}
